package moip;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
	
	public static void main(String[] args) {
		
		if (args.length == 0) {
			System.out.println("Missing argument: log file name");
			return;
		}
		
		// An array of regex patterns, each which used for a specific
		// key/value pair in MOIP logs.
		Pattern[] patterns = {
    			Pattern.compile("level=(.*?) "),
    			Pattern.compile("response_body=\"(.*?)\""),
    			Pattern.compile("request_to=\"(.*?)\""),
    			Pattern.compile("response_headers=map\\[(.*?)\\]\\]"),
    			Pattern.compile("response_status=\"(.*?)\"")
    	};
		
		// Hash-maps to allocate the number of occurrences of each value
		HashMap<String, Integer> request_to_count = new HashMap<>();
		HashMap<String, Integer> response_status_count = new HashMap<>();
		
		try (BufferedReader br = new BufferedReader(new FileReader(args[0]))) {
			
			// Populates the hash-maps...
		    String line = null;
		    while ((line = br.readLine()) != null) {
		    	// Matchers for each searched case
		    	Matcher
		    		request_to_matcher		= patterns[2].matcher(line),
		    		response_status_matcher	= patterns[4].matcher(line);
		    	// Isolates the actual values for [request_to] 
			    if (request_to_matcher.find()) {
			    	String str = request_to_matcher.group(0).split("=\"")[1];
			    	str = str.substring(0, str.length() - 1);
			    	Integer prev_value = request_to_count.get(str);
			    	request_to_count.put(str, prev_value != null ? prev_value + 1 : 1);
			    }
			    // Isolates the actual values for [response_status]
			    if (response_status_matcher.find()) {
			    	String str = response_status_matcher.group(0).split("=\"")[1];
			    	str = str.substring(0, str.length() - 1);
			    	Integer prev_value = response_status_count.get(str);
			    	response_status_count.put(str, prev_value != null ? prev_value + 1 : 1);
			    }
		    }
		    
		    // Order and print the three most called URLs
		    System.out.println("=== 3 Most Called URL's ===");
		    request_to_count.entrySet().stream()
	        	.sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
	        	.limit(3)
	        	.forEach(i -> System.out.println(i.toString().replace("=", " - ")));
		    
		    // Order and print the number of occurrences of each response status
		    System.out.println("=== Response Status ===");
		    response_status_count.entrySet().stream()
	        	.sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
	        	.forEach(i -> System.out.println(i.toString().replace("=", " - ") + " occurrences"));
		    
		} catch (FileNotFoundException e) {
			System.out.println(
					"MOIP log file was not found: " +
					e.getLocalizedMessage()
			);
		} catch (IOException e) {
			System.out.println(
					"Your file appears to be out of the needed format: " +
					e.getLocalizedMessage()
			);
		}
		
	}

}
