# README #

Este projeto é a solução proposta para o teste de desenvolvimento do MOIP, o qual é parte do seu processo seletivo de admissão.

### Problema proposto ###

O Moip tem uma aplicação que envia webhooks para os ecommerces de seus clientes, esses webhooks possuem informações sobre pagamentos (se foram autorizados, cancelados, etc).

Esta aplicação gera logs bastante grandes, precisamos descobrir através do log quem são os clientes que mais recebem os webhooks e verificar todos o response status retornados pelo servidores dos clientes.

O arquivo de log em anexo contém informações de envio de webhooks no format:

	level=info response_body="" request_to"<url>" response_headers= response_status="<code>"

Onde:

* url: é a url para onde foi enviado o webhook;

* code: é o status code retornado pelo servidor do cliente.

As outras informações são irrelevantes para esta task.

Você deve parsear o arquivo e no final mostrar as seguintes informações na saída:

* 3 urls mais chamadas com a quantidade;

* Uma tabela mostrando a quantidade de webhooks por status.

Ex:

	https://woodenoyster.com.br - 100
	https://grotesquemoon.de - 99
	https://notoriouslonesome.com - 90

	200 - 100
	201 - 99

ps: o resultado acima não é o real.

### Ferramentas e Ambiente Utilizado ###

* O [Eclipse Mars.2](https://eclipse.org/mars/) foi a ferramenta escolhida como IDE de execução por ser open-source e de fácil download/configuração;

* Para a implementação da solução foi escolhida a linguagem [Java 8](https://www.java.com/pt_BR/download/). Esta escolha se deve à natureza do problema, onde o principal objetivo é parsear as linhas dos logs e contar ocorrências. O Java, para este problema, oferece um bom set de bibliotecas (HashMap, Leituras de Streams, Regex, etc) e uma sintaxe relativamente simples;

* O Sistema Operacional OSX 10.12.4 foi escolhido por mera preferência.

### Solução Proposta ###

O algoritmo da solução pode ser dividido em três fases:

* Leitura do arquivo de log, linha a linha, identificando os valores de cada chave (*level*, *response_body*, *request_to*, *response_headers*, *response_status*). Os valores foram identificados e contados, onde expressões regulares (*regex*) foram utilizadas para extrair tais informações das linhas. Em cada leitura, a contagem é armazenada em um *HashMap*;

* Após a identificação e contagem de cada uma das chaves, os *HashMaps* agora possuem o número de ocorrências de cada um dos valores delas. A segunda fase ordena estes *HashMap* tendo como critério de ordenamento o número das ocorrências (valores);

* A última fase apenas realiza a tarefa de imprimir os resultados obtidos.

Considerações: é observável que o problema proposto possui múltiplas soluções possíveis. A solução proposta nesta implementação busca manter o máximo possível da estruturação dos registros. A leitura linha à linha mantém a lógica de que cada linha é um registro gravado, enquanto que o parsing destas linhas busca manter a consistência destes registros. Teria sido possível a leitura do log como uma única stream, no intuito de acelerar o processamento, mas caso um único registro viesse a estar corrompido, o processamento inteiro ficaria comprometido. Também teria sido possível ler o log por tokens, mas esta prática poderia gerar uma contagem incorreta dos registros no final, caso uma dada chave estivesse faltando em um dos registros.

### Como Executar a Solução ###

Para executar a solução, é necessário possuir o java 8 instalado na máquina. É possível executar o projeto de duas maneiras, por meio do Eclipse ou por linha de comando:

* Executar com Eclipse: basta importar o diretório *WebhookParser* como projeto do Eclipse e em seguida rodá-lo;

* Executar com linha de comando: tendo o .jar e o arquivo de log no mesmo diretório (os dois estão no diretório *Release*), abra o console e navegue até sua localização. Execute o arquivo jar e passe o nome do arquivo log como parâmetro, como abaixo.

Linha de comando para executar o jar:

	$ java -jar WebhookLogParser.jar log.txt

### Screenshots ###

![Installation Instructions](https://bytebucket.org/mthama/testemoip/raw/73359c2bc156048fc71afaea9dbd184e20ffe833/README.img/screenshot.png?token=3aeacd29338c751e0b76623f7247dce9ea997d49)